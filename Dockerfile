#Image Name
FROM httpd:2.4

#Labels
LABEL maintainer="Jaypee Tello <jaypee.tello@dawsoncollege.qc.ca>"
LABEL description="This Docker image contains an Apache HTTP Server configured with your application."

#Working Directory
WORKDIR /usr/local/apache2/htdocs/

#Copied File Contents
COPY ./app/ /usr/local/apache2/htdocs/

